import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ConfigService} from "../config.service";

@Component({
  selector: 'create-module',
  templateUrl: './create-module.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class CreateModuleComponent {
  @Output() onConfirm = new EventEmitter();

  public projectName = '';


  constructor(private modalService: NgbModal) {
  }

  openWindowCustomClass(content) {
    this.modalService.open(content);
  }

  createProject() {
    this.onConfirm.emit(this.projectName);
  }
}



