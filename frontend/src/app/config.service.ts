import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Project} from "./project";
import { catchError, map, tap } from 'rxjs/operators';

// const httpOptions = {

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient) {
  }

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>('http://localhost:3005/project')
      .pipe(
        tap(projects => console.log('get projects resolved')),
        catchError(this.handleError('getProjects', []))
      )
  }

  createProject(name:string): Observable<Project> {
    return this.http.post<Project>('http://localhost:3005/project', {name: name})
      .pipe(
      tap((projects: Project) => console.log('new project created')),
      catchError(this.handleError<Project>('createProject'))
    );
  }

  deleteProjectById(id:string): Observable<Project>{
    return this.http.delete<Project>('http://localhost:3005/project/'+ id)
      .pipe(
        tap((projects:Project) => console.log('project deleted')),
        catchError(this.handleError<Project>('deleteProjectById'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      //  send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}






// getProjects():Promise<Project[]> {
//     return new Promise((resolve, reject) => {
//       try {
//         this.http.get<Project[]>('http://localhost:3005/project')
//           .subscribe((projects: Project[]) => {
//             resolve(projects);
//           })
//       } catch (e) {
//         reject(e);
//       }
//     })
//   }


// createProject(name: string) {
//   return new Promise((resolve, reject) => {
//     try {
//       this.http.post<Project[]>('http://localhost:3005/project', {name: name})
//         .subscribe((projects: Project[]) => {
//           resolve(projects);
//         })
//     } catch (e) {
//       reject(e);
//     }
//   })
// }

// deleteProjectById(id: string) {
//   return new Promise((resolve, reject) => {
//     try {
//       this.http.delete<Project[]>('http://localhost:3005/project/' + id)
//         .subscribe((projects: Project[]) => {
//           resolve(projects);
//         })
//     } catch (e) {
//       reject(e);
//     }
//   })
// }

