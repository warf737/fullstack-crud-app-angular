import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../config.service';
import {Project} from "../project";


@Component({
  selector: 'show-projects',
  templateUrl: './show-all-projects.component.html',
  styleUrls: ['./show-all-projects.component.css']
})
export class ShowAllProjectsComponent {

  projects: Project[];

  constructor(private myApiService: ConfigService) {
  }


  ngOnInit() {
    this.getProjects();
  }

  getProjects(): void {
    this.myApiService.getProjects()
      .subscribe(projects => this.projects = projects);
  }

  createProject(projectName) {
    this.myApiService.createProject(projectName)
      .subscribe(project => {
        this.projects.push(project);
      });
  }

  deleteProject(_id: string) {
    this.myApiService.deleteProjectById(_id)
      .subscribe(projects => this.getProjects());
  }
}
