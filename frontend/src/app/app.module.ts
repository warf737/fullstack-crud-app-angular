import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from  '@angular/common/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ShowAllProjectsComponent } from './show-all-projects/show-all-projects.component';
import { SingleProjectComponent } from './single-project/single-project.component';
import { CreateModuleComponent } from './create-module/create-module.component';
import { AngularResizedEventModule } from 'angular-resize-event';

@NgModule({
  declarations: [
    AppComponent,
    ShowAllProjectsComponent,
    SingleProjectComponent,
    CreateModuleComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularResizedEventModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
