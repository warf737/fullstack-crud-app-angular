import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ConfigService} from "../config.service";


@Component({
  selector: 'single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.css']
})

export class SingleProjectComponent {

  @Input() project;
  @Input() projects;
  @Output() onDeleted= new EventEmitter();


  constructor() {
  }


  deleteProject() {
    this.onDeleted.emit(this.project._id);
  }
}



















