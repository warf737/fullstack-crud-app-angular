var express = require("express"),
    app = express(),
    cors = require('cors'),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose");


//промежуточный обработчик для междоменных запросов
app.use(cors());
//промежуточный обработчик для входящих post-запросов с данными в теле x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
//промежуточный обработчик для входящих  post-запросов с json-данными
app.use(bodyParser.json());


mongoose.Promise = global.Promise;

//соединение с бд
var connectToMongoBD = function () {
    mongoose.connect("mongodb://localhost:27017/db").then(() => {
        console.log("Connected to Database");
    }).catch((err) => {
        console.log("Not Connected to Database! - retrying connect in 5 sec", err);
        setTimeout(connectToMongoBD, 5000);
    });
};
//подключение к бд
connectToMongoBD();

//создадим схему, используя метод Schema
const projectScheme = new mongoose.Schema({
    id: String,
    name: String
});

//создаем модель проекта c именем Project, используя схему projectScheme
const Project = mongoose.model("Project", projectScheme);


const callbac = function (err, Project) {
    if (err) return console.log(err);
    console.log("update ok");
    res.json(Project);
};

// получает добавленый проект
app.get("/project", async function (req, res) {
    try {
        const projects = await Project.find({});
        res.json(projects);
    } catch (e) {
        console.log(e);
        return e;
    }

});

app.post ("/project", async function (req,res){
    try {
        let project = new Project();
        project.name = req.body.name;
        await project.save();
        res.json(project);
        console.log("project added:", project.name)
    } catch (e) {
        console.log(e);
        return e;
    }
});

//удаляет проекты из бд
app.delete("/project/:id", async (req, res) => {
    try {
        const _id = req.params.id;
        const project = await Project.findOne({_id});
        if (!project) {
            res.json({error: "Project not found"});
        } else {
            await project.remove();
            res.json({message: "deleted"});
        }
    } catch (e) {
        console.error(e);
        res.json({error: e});
    }

});

//слушать запросы на 3005 порту
app.listen(3005, function () {
    console.log("server has started 3005");
});









